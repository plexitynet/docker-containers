#!/bin/bash

### Copy files into specific directories.

### Apache.

if [ -f '/etc/conf/apache/apache2.conf' ]; then
  scp /etc/conf/apache/apache2.conf /etc/apache2/apache2.conf
fi

### PHP.

if [ -f '/etc/conf/php/php.ini' ]; then
  scp /etc/conf/php/php.ini /etc/php5/apache2/php.ini
  scp /etc/conf/php/php.ini /etc/php5/cli/php.ini
fi

### Mysql.

if [ -f '/etc/conf/mysql/my.cnf' ]; then
  scp /etc/conf/mysql/my.cnf /etc/mysql/my.cnf
fi
chown -R mysql:mysql /var/lib/mysql

### Cron.

if [ -d '/etc/conf/cron' ]; then
  scp /etc/conf/cron/* /etc/cron.d/
fi

### Nullmailer.

if [ ! -z "${NULLMAILER_REMOTE}" ]; then
  echo ${NULLMAILER_REMOTE} > /etc/nullmailer/remotes
fi
mkfifo /var/spool/nullmailer/trigger
chmod 0622 /var/spool/nullmailer/trigger
chown mail:root /var/spool/nullmailer/trigger

### Rsyslog.

if [ -f '/etc/conf/rsyslog/rsyslog.conf' ]; then
  scp /etc/conf/rsyslog/rsyslog.conf /etc/rsyslog.conf
fi

### Supervisord.

supervisord -n -c /etc/supervisord.conf
