PleixtyNet Docker containers
============================

Docker containers providing the following:

* *base12* - A base image for other images (based on Ubuntu 12.04).
* *base14* - A base image for other images (based on Ubuntu 14.04).
* *lamp53* - A LAMP stack with PHP 5.3 that can run Drupal 6 and 7 sites.
* *lamp55* - A LAMP stack with PHP 5.5 that can run Drupal 8 sites.
