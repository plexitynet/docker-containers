#!/bin/bash

### Rsyslog.

if [ -d '/etc/conf/rsyslog' ]; then
  scp /etc/conf/rsyslog/* /etc/
fi

### Supervisord.

supervisord -n -c /etc/supervisord.conf

