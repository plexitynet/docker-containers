# This is a base container for Ubuntu 12.04 based PlexityNet docker containers.

FROM ubuntu:12.04
MAINTAINER Saul Willers <saul@plexitynet.com>

ENV REFRESHED_AT 2018-03-06

### APT.

RUN apt-get update
RUN apt-get -y upgrade

# Keep upstart from complaining.
RUN dpkg-divert --local --rename --add /sbin/initctl
RUN ln -fs /bin/true /sbin/initctl

# Install base packages.
RUN DEBIAN_FRONTEND=noninteractive apt-get -y install \
    fail2ban \
    vim \
    git \
    curl \
    wget \
    pwgen \
    python-setuptools \
    vim-tiny \
    sudo \
    python-software-properties \
    python-pip \
    cron \
    unzip

# Set timezone.
RUN echo "Australia/Brisbane" > /etc/timezone; dpkg-reconfigure -f noninteractive tzdata

### Users.

RUN useradd --create-home --shell /bin/bash --user-group deployer

### Fail2ban.

RUN mkdir /var/run/fail2ban

### Rsyslog.

RUN DEBIAN_FRONTEND=noninteractive apt-get -y install rsyslog rsyslog-doc
RUN sed -i 's/xconsole/console/g' /etc/rsyslog.d/*

### Startup scripts.

ADD ./scripts/start.sh /root/start.sh
RUN chmod 755 /root/start.sh

### Supervisord.

# Must install via pip (not easy_install) as it can access python.org via the
# now required https.
RUN pip install supervisor --index-url=https://pypi.python.org/simple
ADD ./conf/supervisor/supervisord.conf /etc/supervisord.conf
RUN mkdir /var/log/supervisor/

